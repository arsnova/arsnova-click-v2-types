export declare interface IModal {
  dismiss(result?: any): void;

  abort(result?: any): void;

  next(result?: any): void;
}
