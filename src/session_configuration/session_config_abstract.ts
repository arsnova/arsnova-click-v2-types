/*
 * This file is part of ARSnova Click.
 * Copyright (C) 2016 The ARSnova Team
 *
 * ARSnova Click is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ARSnova Click is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ARSnova Click.  If not, see <http://www.gnu.org/licenses/>.*/

import { IMusicSessionConfiguration, INickSessionConfiguration, ISessionConfiguration, ISessionConfigurationSerialized } from './interfaces';
import { MusicSessionConfiguration } from './session_config_music';
import { NickSessionConfiguration } from './session_config_nicks';

export abstract class AbstractSessionConfiguration implements ISessionConfiguration {
  private _music: IMusicSessionConfiguration;

  get music(): IMusicSessionConfiguration {
    return this._music;
  }

  set music(value: IMusicSessionConfiguration) {
    this._music = value;
  }

  private _nicks: INickSessionConfiguration;

  get nicks(): INickSessionConfiguration {
    return this._nicks;
  }

  set nicks(value: INickSessionConfiguration) {
    this._nicks = value;
  }

  private _theme: string;

  get theme(): string {
    return this._theme;
  }

  set theme(value: string) {
    this._theme = value;
  }

  private _readingConfirmationEnabled: boolean;

  get readingConfirmationEnabled(): boolean {
    return this._readingConfirmationEnabled;
  }

  set readingConfirmationEnabled(value: boolean) {
    this._readingConfirmationEnabled = value;
  }

  private _showResponseProgress: boolean;

  get showResponseProgress(): boolean {
    return this._showResponseProgress;
  }

  set showResponseProgress(value: boolean) {
    this._showResponseProgress = value;
  }

  private _confidenceSliderEnabled: boolean;

  get confidenceSliderEnabled(): boolean {
    return this._confidenceSliderEnabled;
  }

  set confidenceSliderEnabled(value: boolean) {
    this._confidenceSliderEnabled = value;
  }

  protected constructor({
                          music, nicks, theme, readingConfirmationEnabled, showResponseProgress, confidenceSliderEnabled,
                        }) {
    this.music = new MusicSessionConfiguration(music);
    this.nicks = new NickSessionConfiguration(nicks);
    this.theme = theme;
    this.readingConfirmationEnabled = readingConfirmationEnabled;
    this.showResponseProgress = showResponseProgress;
    this.confidenceSliderEnabled = confidenceSliderEnabled;
  }

  public serialize(): ISessionConfigurationSerialized {
    return {
      music: this.music.serialize(),
      nicks: this.nicks.serialize(),
      theme: this.theme,
      readingConfirmationEnabled: this.readingConfirmationEnabled,
      showResponseProgress: this.showResponseProgress,
      confidenceSliderEnabled: this.confidenceSliderEnabled,
    };
  }

  public equals(value: ISessionConfiguration): boolean {
    return this.music.equals(value.music) && this.nicks.equals(value.nicks) && this.theme === value.theme && this.readingConfirmationEnabled
           === value.readingConfirmationEnabled && this.showResponseProgress === value.showResponseProgress && this.confidenceSliderEnabled
           === value.confidenceSliderEnabled;
  }
}
