import { ISessionConfiguration, ISessionConfigurationSerialized } from './interfaces';
import { AbstractSessionConfiguration } from './session_config_abstract';

export class SessionConfiguration extends AbstractSessionConfiguration implements ISessionConfiguration {
  constructor(options?: ISessionConfigurationSerialized) {
    super(options);
  }
}
