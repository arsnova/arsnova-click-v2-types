import {
  IMusicSessionConfiguration,
  IMusicSessionConfigurationSerialized,
  ITitleMusicSessionConfiguration,
  ITitleMusicSessionConfigurationSerialized,
  IVolumeMusicSessionConfiguration,
  IVolumeMusicSessionConfigurationSerialized,
} from './interfaces';

class VolumeConfiguration implements IVolumeMusicSessionConfiguration {
  private _global: number;

  get global(): number {
    return this._global;
  }

  set global(value: number) {
    this._global = value;
  }

  private _lobby: number;

  get lobby(): number {
    return this._lobby;
  }

  set lobby(value: number) {
    this._lobby = value;
  }

  private _countdownRunning: number;

  get countdownRunning(): number {
    return this._countdownRunning;
  }

  set countdownRunning(value: number) {
    this._countdownRunning = value;
  }

  private _countdownEnd: number;

  get countdownEnd(): number {
    return this._countdownEnd;
  }

  set countdownEnd(value: number) {
    this._countdownEnd = value;
  }

  private _useGlobalVolume: boolean;

  get useGlobalVolume(): boolean {
    return this._useGlobalVolume;
  }

  set useGlobalVolume(value: boolean) {
    this._useGlobalVolume = value;
  }

  constructor({
                global, lobby, countdownRunning, countdownEnd, useGlobalVolume,
              }) {
    this._global = global;
    this._lobby = lobby;
    this._countdownRunning = countdownRunning;
    this._countdownEnd = countdownEnd;
    this._useGlobalVolume = useGlobalVolume;
  }

  public serialize(): IVolumeMusicSessionConfigurationSerialized {
    return {
      global: this.global,
      useGlobalVolume: this.useGlobalVolume,
      lobby: this.lobby,
      countdownRunning: this.countdownRunning,
      countdownEnd: this.countdownEnd,
    };
  }
}

class TitleConfiguration implements ITitleMusicSessionConfiguration {
  private _lobby: string;

  get lobby(): string {
    return this._lobby;
  }

  set lobby(value: string) {
    this._lobby = value;
  }

  private _countdownRunning: string;

  get countdownRunning(): string {
    return this._countdownRunning;
  }

  set countdownRunning(value: string) {
    this._countdownRunning = value;
  }

  private _countdownEnd: string;

  get countdownEnd(): string {
    return this._countdownEnd;
  }

  set countdownEnd(value: string) {
    this._countdownEnd = value;
  }

  constructor({
                lobby, countdownRunning, countdownEnd,
              }) {
    this._lobby = lobby;
    this._countdownRunning = countdownRunning;
    this._countdownEnd = countdownEnd;
  }

  public serialize(): ITitleMusicSessionConfigurationSerialized {
    return {
      lobby: this.lobby,
      countdownRunning: this.countdownRunning,
      countdownEnd: this.countdownEnd,
    };
  }
}

export class MusicSessionConfiguration implements IMusicSessionConfiguration {
  public enabled = {
    lobby: true,
    countdownRunning: true,
    countdownEnd: true,
  };

  private _volumeConfig: IVolumeMusicSessionConfiguration;

  get volumeConfig(): IVolumeMusicSessionConfiguration {
    return this._volumeConfig;
  }

  set volumeConfig(value: IVolumeMusicSessionConfiguration) {
    this._volumeConfig = value;
  }

  private _titleConfig: ITitleMusicSessionConfiguration;

  get titleConfig(): ITitleMusicSessionConfiguration {
    return this._titleConfig;
  }

  set titleConfig(value: ITitleMusicSessionConfiguration) {
    this._titleConfig = value;
  }

  constructor({
                volumeConfig, titleConfig, enabled,
              }) {
    this.volumeConfig = new VolumeConfiguration(volumeConfig);
    this.titleConfig = new TitleConfiguration(titleConfig);
    this.enabled = enabled;
  }

  public serialize(): IMusicSessionConfigurationSerialized {
    return {
      enabled: this.enabled,
      volumeConfig: this.volumeConfig.serialize(),
      titleConfig: this.titleConfig.serialize(),
    };
  }

  public equals(value: IMusicSessionConfiguration): boolean {
    return (
      this.volumeConfig.global === value.volumeConfig.global && //
      this.volumeConfig.lobby === value.volumeConfig.lobby && //
      this.volumeConfig.countdownRunning === value.volumeConfig.countdownRunning && //
      this.volumeConfig.countdownEnd === value.volumeConfig.countdownEnd && //
      this.titleConfig.lobby === value.titleConfig.lobby && //
      this.titleConfig.countdownRunning === value.titleConfig.countdownRunning && //
      this.titleConfig.countdownEnd === value.titleConfig.countdownEnd && //
      this.enabled.lobby === value.enabled.lobby && //
      this.enabled.countdownRunning === value.enabled.countdownRunning && //
      this.enabled.countdownEnd === value.enabled.countdownEnd //
    );
  }
}
