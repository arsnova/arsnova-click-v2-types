export declare interface IVolumeMusicSessionConfigurationSerialized {
  global: number;
  lobby: number;
  countdownRunning: number;
  countdownEnd: number;
  useGlobalVolume: boolean;
}

export declare interface IVolumeMusicSessionConfiguration extends IVolumeMusicSessionConfigurationSerialized {
  serialize(): IVolumeMusicSessionConfigurationSerialized;
}

export declare interface ITitleMusicSessionConfigurationSerialized {
  lobby: string;
  countdownRunning: string;
  countdownEnd: string;
}

export declare interface ITitleMusicSessionConfiguration extends ITitleMusicSessionConfigurationSerialized {
  serialize(): ITitleMusicSessionConfigurationSerialized;
}

export declare interface IMusicSessionConfigurationSerialized {
  enabled: {
    lobby: boolean; countdownRunning: boolean; countdownEnd: boolean;
  };
  volumeConfig: IVolumeMusicSessionConfigurationSerialized;
  titleConfig: ITitleMusicSessionConfigurationSerialized;
}

export declare interface IMusicSessionConfiguration extends IMusicSessionConfigurationSerialized {
  volumeConfig: IVolumeMusicSessionConfiguration;
  titleConfig: ITitleMusicSessionConfiguration;

  serialize(): IMusicSessionConfigurationSerialized;

  equals(value: IMusicSessionConfigurationSerialized): boolean;
}

export declare interface INickSessionConfigurationSerialized {
  memberGroups: Array<string>;
  maxMembersPerGroup: number;
  autoJoinToGroup: boolean;
  selectedNicks: Array<string>;
  blockIllegalNicks: boolean;
  restrictToCasLogin: boolean;
}

export declare interface INickSessionConfiguration extends INickSessionConfigurationSerialized {
  serialize(): INickSessionConfigurationSerialized;

  equals(value: INickSessionConfigurationSerialized): boolean;

  hasSelectedNick(nickname: string): boolean;

  toggleSelectedNick(nickname: string): void;

  addSelectedNick(newSelectedNick: string): void;

  removeSelectedNickByName(selectedNick: string): void;
}

export declare interface ISessionConfigurationSerialized {
  music: IMusicSessionConfigurationSerialized;
  nicks: INickSessionConfigurationSerialized;
  theme: string;
  readingConfirmationEnabled: boolean;
  showResponseProgress: boolean;
  confidenceSliderEnabled: boolean;
}

export declare interface ISessionConfiguration extends ISessionConfigurationSerialized {
  music: IMusicSessionConfiguration;
  nicks: INickSessionConfiguration;

  serialize(): ISessionConfigurationSerialized;

  equals(value: ISessionConfiguration): boolean;
}
