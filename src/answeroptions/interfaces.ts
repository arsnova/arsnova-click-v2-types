import { IValidationStackTrace } from '../questions/interfaces';

export declare interface IAnswerOption {
  readonly TYPE: string;

  isCorrect: boolean;
  answerText: string;

  serialize(): any;

  isValid(): boolean;

  getValidationStackTrace(): Array<IValidationStackTrace>;

  equals(answer: IAnswerOption): boolean;
}

export declare interface IFreetextAnswerOptionSerialized {
  configCaseSensitive: boolean;
  configTrimWhitespaces: boolean;
  configUseKeywords: boolean;
  configUsePunctuation: boolean;
}

export declare interface IFreetextAnswerOption extends IAnswerOption, IFreetextAnswerOptionSerialized {

  isCorrectInput(ref: string): boolean;

  setConfig(configIdentifier: string, configValue: boolean): void;

  getConfig(): Array<IFreetextAnswerOptionConfiguration>;

  serialize(): IFreetextAnswerOptionSerialized;

  equals(answerOption: IFreetextAnswerOption): boolean;
}

export declare interface IFreetextAnswerOptionConfiguration {
  configTitle: string;
  configEnabledString: string;
  enabled: boolean;
  id: string;
}
