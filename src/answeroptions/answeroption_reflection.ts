import { DefaultAnswerOption } from './answeroption_default';
import { FreeTextAnswerOption } from './answeroption_freetext';

export const answerOptionReflection = {
  DefaultAnswerOption: ({ answerText, isCorrect }) => {
    return new DefaultAnswerOption({
      answerText,
      isCorrect,
    });
  },
  FreeTextAnswerOption: ({
                           answerText, configCaseSensitive, configTrimWhitespaces, configUseKeywords, configUsePunctuation,
                         }): FreeTextAnswerOption => {
    return new FreeTextAnswerOption({
      answerText,
      configCaseSensitive,
      configTrimWhitespaces,
      configUseKeywords,
      configUsePunctuation,
    });
  },
};
