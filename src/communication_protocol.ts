export enum COMMUNICATION_PROTOCOL_STATUS {
  SUCCESSFUL = 'Successful', //
  FAILED     = 'Failed', //
}

export enum COMMUNICATION_PROTOCOL_MEMBER {
  UPDATED            = 'Updated', //
  UPDATED_RESPONSE   = 'UpdatedResponse', //
  ADDED              = 'Added', //
  REMOVED            = 'Removed', //
  DUPLICATE_RESPONSE = 'DuplicateResponse', //
  INVALID_RESPONSE   = 'InvalidResponse', //
}

export enum COMMUNICATION_PROTOCOL_LOBBY {
  ILLEGAL_NAME       = 'IllegalName', //
  DUPLICATE_LOGIN    = 'DuplicateLogin', //
  CLOSED             = 'Closed', //
  GET_PLAYERS        = 'GetPlayers', //
  INACTIVE           = 'Inactive', //
  ALL_PLAYERS        = 'AllPlayers', //
  UNKNOWN_GROUP      = 'UnknownGroup', //
  CAS_LOGIN_REQUIRED = 'CasLoginRequired', //
  OPENED             = 'Opened', //
}

export enum COMMUNICATION_PROTOCOL_AUTHORIZATION {
  AUTHENTICATE             = 'Authenticate', //
  AUTHENTICATE_STATIC      = 'AuthenticateStatic', //
  NOT_AUTHORIZED           = 'NotAuthorized', //
  AUTHORIZED               = 'Authorized', //
  AUTHENTICATE_AS_OWNER    = 'AuthenticateAsOwner', //
  INSUFFICIENT_PERMISSIONS = 'InsufficientPermissions', //
}

export enum COMMUNICATION_PROTOCOL_THEMES {
  GET_THEMES = 'GetThemes', //
}

export enum COMMUNICATION_PROTOCOL_I18N {
  UPDATE_LANG               = 'UpdateLang', //
  FILE_NOT_FOUND            = 'FileNotFound', //
  INVALID_DATA              = 'InvalidData', //
  INVALID_PROJECT_SPECIFIED = 'InvalidProjectSpecified', //
}

export enum COMMUNICATION_PROTOCOL_CACHE {
  QUIZ_ASSETS = 'QuizAssets', //
}

export enum COMMUNICATION_PROTOCOL_MATHJAX {
  RENDER = 'Render', //
}

export enum COMMUNICATION_PROTOCOL_WEBSOCKET {
  CONNECTED = 'Connected', //
}

export enum COMMUNICATION_PROTOCOL_EXPORT {
  QUIZ_NOT_FOUND = 'QuizNotFound', //
}

export enum COMMUNICATION_PROTOCOL_QUIZ {
  INIT                           = 'Init', //
  POST                           = 'Post', //
  UNAVAILABLE                    = 'Unavailable', //
  UPDATED_SETTINGS               = 'UpdatedSettings', //
  GET_REMAINING_NICKS            = 'GetRemainingNicks', //
  EXISTS                         = 'Exists', //
  SERVER_PASSWORD_REQUIRED       = 'ServerPasswordRequired', //
  TOO_MUCH_ACTIVE_QUIZZES        = 'TooMuchActiveQuizzes', //
  SET_ACTIVE                     = 'SetActive', //
  SET_INACTIVE                   = 'SetInactive', //
  IS_INACTIVE                    = 'IsInactive', //
  NEXT_QUESTION                  = 'NextQuestion', //
  START                          = 'Start', //
  STOP                           = 'Stop', //
  UNDEFINED                      = 'Undefined', //
  AVAILABLE                      = 'Available', //
  READING_CONFIRMATION_REQUESTED = 'ReadingConfirmationRequested', //
  CONFIDENCE_VALUE_REQUESTED     = 'ConfidenceValueRequested', //
  RESET                          = 'Reset', //
  INVALID_PARAMETERS             = 'InvalidParameters', //
  UPLOAD_FILE                    = 'UploadFile', //
  ALREADY_STARTED                = 'AlreadyStarted', //
  END_OF_QUESTIONS               = 'EndOfQuestions', //
  CURRENT_STATE                  = 'CurrentState', //
  GET_STARTTIME                  = 'GetStartTime', //
  GET_LEADERBOARD_DATA           = 'GetLeaderboardData', //
  RESERVED                       = 'Reserved', //
  REMOVED                        = 'Removed', //
}

export const COMMUNICATION_PROTOCOL = {
  STATUS: COMMUNICATION_PROTOCOL_STATUS, //
  MEMBER: COMMUNICATION_PROTOCOL_MEMBER, //
  LOBBY: COMMUNICATION_PROTOCOL_LOBBY, //
  AUTHORIZATION: COMMUNICATION_PROTOCOL_AUTHORIZATION, //
  THEMES: COMMUNICATION_PROTOCOL_THEMES, //
  I18N: COMMUNICATION_PROTOCOL_I18N, //
  CACHE: COMMUNICATION_PROTOCOL_CACHE, //
  MATHJAX: COMMUNICATION_PROTOCOL_MATHJAX, //
  WEBSOCKET: COMMUNICATION_PROTOCOL_WEBSOCKET, //
  EXPORT: COMMUNICATION_PROTOCOL_EXPORT, //
  QUIZ: COMMUNICATION_PROTOCOL_QUIZ, //
};
