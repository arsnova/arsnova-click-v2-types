import { DefaultAnswerOption } from '../answeroptions/answeroption_default';
import { IQuestionChoice } from './interfaces';
import { SingleChoiceQuestion } from './question_choice_single';

export class TrueFalseSingleChoiceQuestion extends SingleChoiceQuestion implements IQuestionChoice {

  public canEditQuestionText = true;
  public canEditAnsweroptions = true;
  public canEditQuestionTimer = true;
  public canEditQuestionType = true;

  public canAddAnsweroptions = false;

  public readonly preferredAnsweroptionComponent: string = 'AnsweroptionsDefaultComponent';

  public TYPE = 'TrueFalseSingleChoiceQuestion';

  /**
   * Constructs a TrueFalseSingleChoiceQuestion instance
   * @see AbstractChoiceQuestion.constructor()
   * @param options
   */
  constructor(options) {
    super(options);
    if (!options.answerOptionList.length) {
      this.answerOptionList = [
        new DefaultAnswerOption({
          answerText: '',
          isCorrect: true,
        }), new DefaultAnswerOption({
          answerText: '',
          isCorrect: false,
        }),
      ];
    }
  }

  /**
   * Serialize the instance object to a JSON compatible object
   * @returns {{hashtag:String,questionText:String,type:AbstractQuestion,timer:Number,startTime:Number,questionIndex:Number,answerOptionList:Array}}
   */
  public serialize(): Object {
    return Object.assign(super.serialize(), { TYPE: this.TYPE });
  }

  public translationReferrer(): string {
    return 'component.questions.single_choice_question_true_false';
  }

  public translationDescription(): string {
    return 'component.question_type.description.TrueFalseSingleChoiceQuestion';
  }

  public removeAnswerOption(): void {
    throw Error('AnswerOptions cannot be modified for this type of Question!');
  }

  public addDefaultAnswerOption(): void {
    throw Error('AnswerOptions cannot be modified for this type of Question!');
  }
}
