import { MultipleChoiceQuestion } from './question_choice_multiple';
import { SingleChoiceQuestion } from './question_choice_single';
import { ABCDSingleChoiceQuestion } from './question_choice_single_abcd';
import { TrueFalseSingleChoiceQuestion } from './question_choice_single_true_false';
import { YesNoSingleChoiceQuestion } from './question_choice_single_yes_no';
import { FreeTextQuestion } from './question_freetext';
import { RangedQuestion } from './question_ranged';
import { SurveyQuestion } from './question_survey';

export const questionReflection = {
  SingleChoiceQuestion: ({ questionText, timer, displayAnswerText, answerOptionList, showOneAnswerPerRow }): SingleChoiceQuestion => {
    return new SingleChoiceQuestion({
      questionText,
      timer,
      displayAnswerText,
      answerOptionList,
      showOneAnswerPerRow,
    });
  },
  ABCDSingleChoiceQuestion: ({ questionText, timer, displayAnswerText, answerOptionList, showOneAnswerPerRow }): YesNoSingleChoiceQuestion => {
    return new ABCDSingleChoiceQuestion({
      questionText,
      timer,
      displayAnswerText,
      answerOptionList,
      showOneAnswerPerRow,
    });
  },
  YesNoSingleChoiceQuestion: ({ questionText, timer, displayAnswerText, answerOptionList, showOneAnswerPerRow }): YesNoSingleChoiceQuestion => {
    return new YesNoSingleChoiceQuestion({
      questionText,
      timer,
      displayAnswerText,
      answerOptionList,
      showOneAnswerPerRow,
    });
  },
  TrueFalseSingleChoiceQuestion: ( //
    { questionText, timer, displayAnswerText, answerOptionList, showOneAnswerPerRow }): TrueFalseSingleChoiceQuestion => {
    return new TrueFalseSingleChoiceQuestion({
      questionText,
      timer,
      displayAnswerText,
      answerOptionList,
      showOneAnswerPerRow,
    });
  },
  MultipleChoiceQuestion: ({ questionText, timer, displayAnswerText, answerOptionList, showOneAnswerPerRow }): MultipleChoiceQuestion => {
    return new MultipleChoiceQuestion({
      questionText,
      timer,
      displayAnswerText,
      answerOptionList,
      showOneAnswerPerRow,
    });
  },
  SurveyQuestion: ({ questionText, timer, displayAnswerText, answerOptionList, showOneAnswerPerRow, multipleSelectionEnabled }): SurveyQuestion => {
    return new SurveyQuestion({
      questionText,
      timer,
      displayAnswerText,
      answerOptionList,
      showOneAnswerPerRow,
      multipleSelectionEnabled,
    });
  },
  RangedQuestion: ({ questionText, timer, rangeMin, rangeMax, correctValue }): RangedQuestion => {
    return new RangedQuestion({
      questionText,
      timer,
      rangeMin,
      rangeMax,
      correctValue,
    });
  },
  FreeTextQuestion: ({ questionText, timer, answerOptionList }): FreeTextQuestion => {
    return new FreeTextQuestion({
      questionText,
      timer,
      answerOptionList,
    });
  },
};


