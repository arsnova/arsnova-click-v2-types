import { ISessionConfiguration } from '../session_configuration/interfaces';
import { SessionConfiguration } from '../session_configuration/session_config';
import { IQuestion } from './interfaces';
import { DefaultQuestionGroup } from './questiongroup_default';

export const questionGroupReflection = {
  DefaultQuestionGroup: ({
                           hashtag, questionList, isFirstStart, sessionConfig,
                         }: {
    hashtag: string, questionList?: Array<IQuestion>, isFirstStart?: boolean, sessionConfig?: ISessionConfiguration
  }): DefaultQuestionGroup => {
    return new DefaultQuestionGroup({
      hashtag: hashtag,
      questionList: questionList || [],
      isFirstStart: isFirstStart || true,
      sessionConfig: new SessionConfiguration(sessionConfig) || new SessionConfiguration(null),
    });
  },
};


