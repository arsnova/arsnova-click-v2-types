import { IQuestionChoice } from './interfaces';
import { SingleChoiceQuestion } from './question_choice_single';

export class ABCDSingleChoiceQuestion extends SingleChoiceQuestion implements IQuestionChoice {

  public canEditQuestionText = true;
  public canEditAnsweroptions = true;
  public canEditQuestionTimer = true;
  public canEditQuestionType = true;

  public canAddAnsweroptions = false;

  public readonly preferredAnsweroptionComponent: string = 'AnsweroptionsDefaultComponent';

  public TYPE = 'ABCDSingleChoiceQuestion';

  /**
   * Constructs a AbcdSingleChoiceQuestion instance
   * @see AbstractChoiceQuestion.constructor()
   * @param options
   */
  constructor(options) {
    super(options);
  }

  public isValid(): boolean {
    return true;
  }

  /**
   * Serialize the instance object to a JSON compatible object
   * @returns {{hashtag:String,questionText:String,type:AbstractQuestion,timer:Number,startTime:Number,questionIndex:Number,answerOptionList:Array}}
   */
  public serialize(): Object {
    return Object.assign(super.serialize(), { TYPE: this.TYPE });
  }

  public translationReferrer(): string {
    return 'component.questions.single_choice_question_abcd';
  }

  public translationDescription(): string {
    return 'component.question_type.description.AbcdSingleChoiceQuestion';
  }

  public removeAnswerOption(): void {
    throw Error('AnswerOptions cannot be modified for this type of Question!');
  }

  public addDefaultAnswerOption(): void {
    throw Error('AnswerOptions cannot be modified for this type of Question!');
  }
}
