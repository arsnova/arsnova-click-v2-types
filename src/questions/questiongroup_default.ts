import { ISessionConfiguration } from '../session_configuration/interfaces';
import { SessionConfiguration } from '../session_configuration/session_config';
import { IQuestion, IQuestionGroup } from './interfaces';
import { AbstractQuestion } from './question_abstract';
import { questionReflection } from './question_reflection';
import { AbstractQuestionGroup } from './questiongroup_abstract';

export class DefaultQuestionGroup extends AbstractQuestionGroup implements IQuestionGroup {

  public TYPE = 'DefaultQuestionGroup';

  constructor({
                hashtag, questionList = [], isFirstStart = true, sessionConfig = new SessionConfiguration(null),
              }: {
    hashtag: string, questionList?: Array<IQuestion>, isFirstStart?: boolean, sessionConfig?: ISessionConfiguration
  }) {
    super({
      hashtag,
      questionList,
      isFirstStart,
      sessionConfig,
    });
    if (this.questionList.length > 0) {
      this.questionList.forEach((question: IQuestion, index: number) => {
        if (!(
          question instanceof AbstractQuestion
        )) {
          this.questionList[index] = questionReflection[question.TYPE](question);
        }
      }, this);
    }
  }

  /**
   * Serialize the instance object to a JSON compatible object
   * @returns {{hashtag: String, type: String, questionList: Array}}
   */
  public serialize(): any {
    return Object.assign(super.serialize(), { TYPE: this.TYPE });
  }
}
