import { IQuestionChoice, IValidationStackTrace } from './interfaces';
import { AbstractChoiceQuestion } from './question_choice_abstract';

export class SingleChoiceQuestion extends AbstractChoiceQuestion implements IQuestionChoice {

  public canEditQuestionText = true;
  public canEditAnsweroptions = true;
  public canEditQuestionTimer = true;
  public canEditQuestionType = true;

  public canAddAnsweroptions = true;

  public readonly preferredAnsweroptionComponent: string = 'AnsweroptionsDefaultComponent';

  public TYPE = 'SingleChoiceQuestion';

  /**
   * Constructs a SingleChoiceQuestion instance
   * @see AbstractChoiceQuestion.constructor()
   * @param options
   */
  constructor({ questionText, timer, displayAnswerText, answerOptionList, showOneAnswerPerRow }: any) {
    super({
      questionText,
      timer,
      displayAnswerText,
      answerOptionList,
      showOneAnswerPerRow,
    });
  }

  /**
   * Serialize the instance object to a JSON compatible object
   * @returns {
   *  {hashtag:String,questionText:String,type:AbstractQuestion,timer:Number,startTime:Number,questionIndex:Number,answerOptionList:Array}
   * }
   */
  public serialize(): Object {
    return Object.assign(super.serialize(), { TYPE: this.TYPE });
  }

  /**
   * Checks if the properties of this instance are valid. Checks also recursively all including AnswerOption instances
   * and summarizes their result of calling .isValid(). Checks also if this Question instance contains exactly one correct AnswerOption
   * @see AbstractChoiceQuestion.isValid()
   * @returns {boolean} True, if the complete Question instance is valid, False otherwise
   */
  public isValid(): boolean {
    let hasValidAnswer = 0;
    this.answerOptionList.forEach(answeroption => {
      if (answeroption.isCorrect) {
        hasValidAnswer++;
      }
    });
    return super.isValid() && hasValidAnswer === 1;
  }

  /**
   * Gets the validation error reason from the question and all included answerOptions as a stackable array
   * @returns {Array} Contains an Object which holds the number of the current question and the reason why the validation has failed
   */
  public getValidationStackTrace(): Array<IValidationStackTrace> {
    let hasValidAnswer = 0;
    this.answerOptionList.forEach(answeroption => {
      if (answeroption.isCorrect) {
        hasValidAnswer++;
      }
    });
    const parentStackTrace = super.getValidationStackTrace();
    if (hasValidAnswer !== 1) {
      parentStackTrace.push({
        occurredAt: { type: 'question' },
        reason: 'one_valid_answer_required',
      });
    }
    return parentStackTrace;
  }

  public translationReferrer(): string {
    return 'component.questions.single_choice_question';
  }

  public translationDescription(): string {
    return 'component.question_type.description.SingleChoiceQuestion';
  }
}
