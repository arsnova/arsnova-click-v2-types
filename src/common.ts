import * as WebSocket from 'ws';
import {
  COMMUNICATION_PROTOCOL_AUTHORIZATION,
  COMMUNICATION_PROTOCOL_CACHE,
  COMMUNICATION_PROTOCOL_EXPORT,
  COMMUNICATION_PROTOCOL_I18N,
  COMMUNICATION_PROTOCOL_LOBBY,
  COMMUNICATION_PROTOCOL_MATHJAX,
  COMMUNICATION_PROTOCOL_MEMBER,
  COMMUNICATION_PROTOCOL_QUIZ,
  COMMUNICATION_PROTOCOL_STATUS,
  COMMUNICATION_PROTOCOL_THEMES,
  COMMUNICATION_PROTOCOL_WEBSOCKET,
} from './communication_protocol';
import { IQuestionGroup } from './questions/interfaces';

export declare interface IAvailableNicks {
  disney: Array<string>;
  science: Array<string>;
  fantasy: Array<string>;
  literature: Array<string>;
  mythology: Array<string>;
  actor: Array<string>;
  politics: Array<string>;
  turing_award: Array<string>;
  emojis: Array<any>;
}

export declare interface ITheme {
  name: string;
  description: string;
  id: string;
}

export declare interface IMathjaxResponse {
  css?: string;
  html?: string;
  svg?: string;
  mml?: string;
  speakText: string;
}

export declare interface ICasData {
  username: Array<string>;
  displayName: Array<string>;
  mail: Array<string>;
}

export declare interface IQuizResponse {
  value: Array<number> | number | string;
  responseTime: number;
  confidence: number;
  readingConfirmation: boolean;
}

export declare interface INicknameSerialized {
  id: number;
  name: string;
  groupName: string;
  colorCode: string;
  responses: Array<IQuizResponse>;
}

export declare interface INickname extends INicknameSerialized {
  webSocketAuthorization: number;
  casProfile: ICasData;
  webSocket?: WebSocket;

  serialize(): INicknameSerialized;
}

export declare interface IMemberGroup extends IMemberGroupSerialized {
  members: Array<INickname>;

  serialize(): IMemberGroupSerialized;
}

export declare interface IMemberGroupSerialized {
  name: string;
  members: Array<INicknameSerialized>;
}

export declare interface IActiveQuizSerialized {
  memberGroups: Array<IMemberGroupSerialized>;
  currentQuestionIndex: number;
  originalObject: IQuestionGroup;
}

export declare interface IActiveQuiz extends IActiveQuizSerialized {
  memberGroups: Array<IMemberGroup>;
  name: string;
  currentStartTimestamp: number;
  ownerSocket: WebSocket;

  addMember(name: string, webSocketAuthorization: number, groupName: string, ticket?: string): boolean;

  removeMember(name: string): boolean;

  addResponseValue(nickname: string, data: Array<number>): void;

  setConfidenceValue(nickname: string, confidenceValue: number): void;

  setReadingConfirmation(nickname: string): void;

  requestReadingConfirmation(): void;

  nextQuestion(): number;

  setTimestamp(startTimestamp: number): void;

  stop(): void;

  reset(): void;

  updateQuizSettings(target: string, state: boolean): void;

  onDestroy(): void;

  serialize(): IActiveQuizSerialized;

  findMemberByName(nickname: string): INickname;
}

export declare interface IQuizResponse {
  value: Array<number> | number | string;
  responseTime: number;
  confidence: number;
  readingConfirmation: boolean;
}

export declare interface IDuplicateQuiz {
  quizName: string;
  fileName: string;
  renameRecommendation: Array<string>;
}

export declare interface ICurrentQuizData {
  quiz: IQuestionGroup;
  questionIndex: number;
  readingConfirmationRequested: boolean;
}

export declare interface ICurrentQuiz extends ICurrentQuizData {
  serialize(): ICurrentQuizData;
}

export declare interface IServerSettings {
  cacheQuizAssets: boolean;
  createQuizPasswordRequired: boolean;
  limitActiveQuizzes: number;
}

export declare interface ISong {
  id: string;
  text: string;
}

export declare type IMessageStep = COMMUNICATION_PROTOCOL_AUTHORIZATION | //
  COMMUNICATION_PROTOCOL_EXPORT | //
  COMMUNICATION_PROTOCOL_QUIZ | //
  COMMUNICATION_PROTOCOL_MEMBER | //
  COMMUNICATION_PROTOCOL_LOBBY | //
  COMMUNICATION_PROTOCOL_CACHE | //
  COMMUNICATION_PROTOCOL_MATHJAX | //
  COMMUNICATION_PROTOCOL_I18N | //
  COMMUNICATION_PROTOCOL_WEBSOCKET | //
  COMMUNICATION_PROTOCOL_THEMES; //

export declare interface IMessage {
  step: IMessageStep;
  status?: COMMUNICATION_PROTOCOL_STATUS;
  payload?: any;
}

export declare interface ILeaderBoardItem {
  name: string;
  responseTime: number;
  score: number;
}

export declare interface ILeaderBoard {
  attendees: Array<ILeaderBoardItem>;
}

export declare interface ILeaderBoardItem {
  name: string;
  responseTime: number;
  correctQuestions: Array<number>;
  confidenceValue: number;
}

export declare interface ILogin extends ILoginSerialized {
  token: string;

  generateToken(): string;

  serialize(): ILoginSerialized;
}

export declare interface ILoginSerialized {
  username: string;
  passwordHash: string;
  gitlabToken: string;
  userAuthorizations: Array<any>;
}

export declare interface IStorageDAO<T> {
  createDump(): T | Array<T>;
}
